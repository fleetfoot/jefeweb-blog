---
title: Storage Warrior
date: 2021-07-26
tags:
  - portfolio
layout: layouts/post.njk
link: https://www.storagewarrior.ca
templateClass: tmpl-post
image: storagewarrior.png
tech:
  - WordPress
  - JavaScript
  - Sass
---

I had a lot of fun building this website. It starts with WordPress Underscores starter theme, Gulp 4 as a task runner, SASS for writing clean BEM styles and Webpack to compile the JavaScript. The WordPress backend uses ACF for page templates and Gutenberg for posts and non-templated pages. I used Cloudflare for caching and SSL, and server-side caching for additional speed.
